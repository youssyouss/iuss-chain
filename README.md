# iuss-chain

# ------- USAGE ------ #


    npm install

    npm start 

to launch the server on port 3001 & peer on 5001

    HTTP_PORT=3002 P2P_PORT=5002 PEERS=ws://localhost:5001 npm start

to launch a second peer

GET localhost:3002/public-key on Postman to get user public key

POST localhost:3001/transact on Postman: 
    body : raw
        {
            "recipient":"public-key",
            "amount":"50"
        }
        
Json example returned : 
    [
    {
        "id": "e7e13b90-06f6-11e9-a08a-430916a768f9",
        "input": {
            "timestamp": 1545599313353,
            "amount": 450,
            "address": "0468e709e3ca39d3d455fe50779df90b312b053d97f098c7f1f2c38137f80f5684eef39a05e1408a52d48c77e08bd9b593d9b499929427624e4160b78e6b43a573",
            "signature": {
                "r": "bc0d2e5f9014e6f4d9253bf361597cb1ef1fdd33c0a6d4a9472082abc7769296",
                "s": "33ede5c188aa81717f04c387bd81e9dae0616204a0a3ff6bf29277cbb1749d67",
                "recoveryParam": 0
            }
        },
        "outputs": [
            {
                "amount": 400,
                "address": "0468e709e3ca39d3d455fe50779df90b312b053d97f098c7f1f2c38137f80f5684eef39a05e1408a52d48c77e08bd9b593d9b499929427624e4160b78e6b43a573"
            },
            {
                "amount": 50,
                "address": "0468e709e3ca39d3d455fe50779df90b312b053d97f098c7f1f2c38137f80f5684eef39a05e1408a52d48c77e08bd9b593d9b499929427624e4160b78e6b43a573"
            }
        ]
    }
    ]


GET http://localhost:3001/transactions

displays transaction in JSON


GET http://localhost:3001/mine-transactions

Miners reward





